import Constants from '../../constants';

export default {
  fmaAuth: (clave) => ({
		type: Constants.REGISTRATION_REQUEST_FMA,
		clave
	}),
  clearFmaAuth: () => ({
	  type: Constants.REGISTRATION_CLEAR_FMA
  })
};
