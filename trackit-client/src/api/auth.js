/* eslint-disable react/react-in-jsx-scope */
import { cssNumber } from 'jquery';
import { call } from './misc.js';
import Config from '../config';

const handleResponse = (response) => {
  if (response.status === 401)
    throw response.statusText;
  return response.json();
};

const verifyCodeLogin = async () => {
  let url = "https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser";
  let headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
  let email = document.getElementById('email');
  let tokenFMAId = document.getElementById('tokenToValidate');
  let codeFMAId = document.getElementById('mfa_code_add_v');
  let wordsFMA = '';
  if(tokenFMAId !== null && codeFMAId !== null){
    return fetch(url, {
      "method": "POST",
      headers,
      "body": JSON.stringify({
        username: email.value,
        TWO_FA_APP_NAME: "trackIt",
        words: wordsFMA,
        token: tokenFMAId.value,
        code: codeFMAId.value,
        operation: 'validate_qr'
      })
    }).then(handleResponse)
      .then(response => ({success: true, data: response }))
      .then(error => ({success: false, ...error}))
      .catch(error => ({success: null, error}));
  }
};

const verify_mfa_exist = async (email, password, awsToken) => {
  let url = "https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser";
  let headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
  return fetch(url, {
    "method": "POST",
    headers,
    "body": JSON.stringify({
      username: email,
      TWO_FA_APP_NAME: "trackIt",
      "operation": "verify_mfa_exist"
    })
  }).then(handleResponse)
    .then(response => ({success: true, data: response }))
    .then(error => ({success: false, ...error}))
    .catch(error => ({success: null, error}));
};

const verifyUserPasswordLogin = async (user, password, awsToken) => {
  let url = Config.apiUrl + "/user/login";
  let headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
  return fetch(url, {
    "method": "POST",
    headers,
    "body": JSON.stringify({
      email: user,
      password: password,
      awsToken: null,
      origin: "trackit"
    })
  }).then(handleResponse)
    .then(response => ({success: true, data: response }))
    .then(error => ({success: false, ...error}))
    .catch(error => ({success: null, error}));
};

const hiddeFields = () => {
  let fieldEmail = document.getElementsByName('email')[0];
  let fieldPassword = document.getElementsByName('password')[0];
  let labelForm = document.getElementsByTagName('label');
  var labelList = Array.prototype.slice.call(labelForm);
  labelList.forEach(element => {
    if(element.innerHTML === "Email address" || element.innerHTML === "Password"){
      element.classList.add("elementhide");
    }
  });

  let anchorForm = document.getElementsByTagName('a');
  var anchorList = Array.prototype.slice.call(anchorForm);
  anchorList.forEach(elementAnchor => {
    if(elementAnchor.innerHTML !== "Return to Login"){
      elementAnchor.classList.add("elementhide");
    }
  });
  fieldEmail.classList.add("elementhide");
  fieldPassword.classList.add("elementhide");
}

export const login = async (email, password, awsToken) => {
  let tokenToValidate = document.getElementById('tokenToValidate');
  let codeFMAId = document.getElementById('mfa_code_add_v');
  let responseVerify = {"success": false};
  if(tokenToValidate.value === ""){
    responseVerify = await verifyUserPasswordLogin(email, password, awsToken)
  }
  
  if(responseVerify.success === true && responseVerify.data.error === undefined){
    
    if(codeFMAId.value !== '' && tokenToValidate.value !== ''){
      let verifyCodeValid = await verifyCodeLogin();
      if(verifyCodeValid.data.body === true){
        return call('/user/login', 'POST', {email, password, awsToken, origin: "trackit"});
      }
    }else{
      let reponseVerifyMfa = await verify_mfa_exist(email, password, awsToken);
      if(reponseVerifyMfa.data.body.error !== undefined){
        return call('/user/login', 'POST', {email, password, awsToken, origin: "trackit"});
      }else{
        let validateDiv  = document.getElementById('add-mfa-step-4');
        
        
        validateDiv.classList.remove('elementhide');
        tokenToValidate.value = reponseVerifyMfa.data.body.token;
        
        hiddeFields();
        return {success: true, data: {}}
      }
    }
    
  }else if(tokenToValidate.value !== '' && codeFMAId.value === ''){
    tokenToValidate.focus();
    return {success: true, data: {}};
  }else{
    let responseLogin = call('/user/login', 'POST', {email, password, awsToken, origin: "trackit"});
    return responseLogin
  }
  
};

export const register = (email, password, awsToken) => {
  return call('/user', 'POST', {email, password, awsToken, origin: "trackit"});
};

export const recoverPassword = (email) => {
  return call('/user/password/forgotten', 'POST', {email, origin: "trackit"});
};

export const renewPassword = (id, password, token) => {
  return call('/user/password/reset', 'POST', {id, password, token});
};
