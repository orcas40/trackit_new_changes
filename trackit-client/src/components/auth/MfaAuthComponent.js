/* eslint-disable react/jsx-no-undef */
import React, {Component} from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Spinner from 'react-spinkit';
import { call } from '../../api/misc.js';

// Form imports
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
//import Validations from '../../common/forms';

import '../../styles/Login.css';
import logo from '../../assets/logo-coloured.png';
//import QRCode from './qrcode'

//const Validation = Validations.Auth;

// MfaAuthComponent Form Component
export class MfaAuthComponent extends Component {

  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  getVariableGetByName() {
    var variables = {};
    var arreglos = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
       variables[key] = value;
    });
    return variables;
 }

  verifyCode= () => {
    let username = this.getVariableGetByName()["email"];
    let tokenFMAId = document.getElementById('tokenFMA');
    let codeFMAId = document.getElementById('mfa_code_add');
    let wordsFMA = document.getElementById('wordsFMA');
    if(tokenFMAId !== null && codeFMAId !== null && wordsFMA !== null){
      fetch("https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser", {
        "method": "POST",
        "headers": {
          "x-api-key": "VXgzfBKDnJ7S8PJhKJSjE47PUMgMQEvlptuF36Nd",
          "content-type": "application/json",
          "accept": "application/json"
        },
        "body": JSON.stringify({
          username: username,
          TWO_FA_APP_NAME: "trackIt",
          words: wordsFMA.value,
          token: tokenFMAId.value,
          code: codeFMAId.value,
          operation: 'validate_qr'
        })
      })
      .then(response => response.json())
      .then(response => {
        // eslint-disable-next-line no-undef
        let validateCode = response.body;
        if(validateCode){
          let step1 = document.getElementById('add-mfa-step-1');
          let step3 = document.getElementById('add-mfa-step-3');
          let wordsStep = document.getElementById('words');
          step1.classList.add('elementhide');
          step3.classList.remove('elementhide');
          wordsStep.innerHTML=(wordsFMA.value);
        }
      })
      .catch(err => {
        console.log(err);
        // eslint-disable-next-line react/jsx-no-undef
        return (<Redirect to="/"/>);
      });
    }
  }
  submit = (e) => {
    console.log('******** submit function ', e);
    return;
    /*e.preventDefault();
    let values = this.form.getValues();
    console.log('values', values)
    console.log('values.mfa_code_add', values.mfa_code_add)
    this.props.submit(values.mfa_code_add);*/
  };

  render() { 
    const buttons = (this.props.renewStatus && this.props.renewStatus.status && this.props.renewStatus.value ? null : (
      <div className="clearfix">
        <Link
          to="/login"
        >
          Return to Login
        </Link>
      </div>
    ));

    const error = (this.props.renewStatus && this.props.renewStatus.hasOwnProperty("error") ? (
      <div className="alert alert-warning">{this.props.renewStatus.error}</div>
      ): "");

    const success = (this.props.renewStatus && this.props.renewStatus.status && this.props.renewStatus.value ? (
      <div className="alert alert-success">
        <strong>Success : </strong>
        Your new password has been set. You may now <Link to="/login/">Sign in</Link>.
      </div>
    ) : null);

    const form = (
      <div>
        <div class="img-login">
          <div id="message-error-add-mfa" class="alert alert-danger alert-dismissible elementhide">
            <a href="#" id="message-error-add-mfa-close" class="close elementhide" aria-label="close">times</a>
            <strong>There was an error with the MFA code you entered. Please try again.</strong>
          </div>
          <div id="message-error-add-mfa-no-code" class="alert alert-danger alert-dismissible elementhide">
            <a href="#" id="message-error-add-mfa-no-code-close" class="close" aria-label="close">times</a>
            <strong>Please enter the 6-digit code found in the authenticator application.</strong>
          </div>
        </div>
        <div id="add-mfa-step-1">
          <p class="subtitle_login">
            You are required to configure multi-factor authentication for this account. Please scan the QR code below with an authenticator application on your mobile device 'such as Google Authenticator'.
          </p>
          <div class="qr_container">
            <canvas id="qr"></canvas>
            <input
            type="hidden"
            id="tokenFMA"
            name="tokenFMA"
            className="form-control"
            disabled
          />
          <input
            type="hidden"
            id="wordsFMA"
            name="wordsFMA"
            className="form-control"
            disabled
          />
          </div>
          <div>Once you have scanned the code, Please enter the 6-digit code shown in the app. Do not close the authenticator application yet. click "Continue".</div>
          <div id="fieldList3">
            <label class="form-control-label">Code</label>
          </div>
          <label for="inputMFACodeAdd" class="sr-only">Code</label>
          <Input
            type="text"
            id="mfa_code_add"
            name="mfa_code_add"
            className="form-control"
          />
          <br/>
          <div class="login_btn">
            <div class="forgot mb-3">
              <button class="btn btn-info" type="button" onClick={this.verifyCode}>Confirm Code</button>
            </div>
          </div>
        </div>
        <div id="add-mfa-step-2" className="elementhide">
          <div className="alert alert-warning">
            This user is active in MFA.
          </div>
          <br />
        </div>
        <div id="add-mfa-step-3" className="elementhide">
          <p class="subtitle_login">
            Please save the following words in a safe place. You will need them in case you ever lose access to your authenticator application and need to reset the MFA device associated with your account. (All the letters below are in lowercase.)
          </p>
          <div className="divFMA2"></div>
          <div class="conteiner_numbers">
            <div id="words" className="divFMA3"></div>
          </div>
          <div className="divFMA2"></div>
        </div>
      </div>
    );

    return (
        <div className="login">
          <div className="row">
            <div className="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 parent">
              <div className="white-box vertCentered">
                <img src={logo} id="logo" alt="TrackIt logo" />
                <hr/>
                {error}
                <Form ref={ (form) => { this.form = form; } } onSubmit={this.submit}>
                  {success || form}
                  {buttons}
                </Form>
              </div>
            </div>
          </div>
        </div>
    )
  }  
}

MfaAuthComponent.propTypes = {
  submit: PropTypes.func.isRequired,
  renewStatus: PropTypes.shape({
    status: PropTypes.bool,
    error: PropTypes.string
  })
};

export default withRouter(MfaAuthComponent);