import Form from './FormComponent';
import ForgotPassword from './ForgotPasswordComponent';
import RenewPassword from './RenewPasswordComponent';
import MfaAuth from './MfaAuthComponent';

export default {
  Form,
  ForgotPassword,
  RenewPassword,
  MfaAuth,
};
