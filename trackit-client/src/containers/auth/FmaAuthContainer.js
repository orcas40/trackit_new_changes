import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from "react-router-dom";
import PropTypes from 'prop-types';
import Components from '../../components';
import Actions from '../../actions/index';
import Config from '../../config';

const Form = Components.Auth.MfaAuth;

const createqr = (user) => {
  let qrCodeString;
  let wordsString;
  let tokenFMA;
  fetch("https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser", {
    "method": "POST",
    "headers": {
      "x-api-key": "VXgzfBKDnJ7S8PJhKJSjE47PUMgMQEvlptuF36Nd",
      "content-type": "application/json",
      "accept": "application/json"
    },
    "body": JSON.stringify({
      username: user,
      TWO_FA_APP_NAME: "trackIt",
      domainFMA: "localhost",
      "operation": "get_qrcode"
    })
  })
  .then(response => response.json())
  .then(response => {
    // eslint-disable-next-line no-undef
    qrCodeString = response.body.token_url;
    // eslint-disable-next-line no-undef
    wordsString = response.body.words;
    tokenFMA = response.body.token;
    console.log('**** entro aqui');
    // eslint-disable-next-line no-undef
    let QRCode = require('qrcode')
    let canvas = document.getElementById('qr')
    let tokenFMAId = document.getElementById('tokenFMA')
    let wordsFMA = document.getElementById('wordsFMA')
    QRCode.toCanvas(canvas, qrCodeString, function (error) {
      if (error) console.error(error)
        console.log('success!');
        tokenFMAId.value = tokenFMA;
        wordsFMA.value = wordsString;
        //this.props.tokenFMA = tokenFMA;
    })
  })
  .catch(err => {
    console.log(err);
    return (<Redirect to="/"/>);
  });
}

const handleResponse = (response) => {
  if (response.status === 401)
    throw response.statusText;
  return response.json();
}; 

const createQrIfNotExist = async (user) => {
  let resultValidate = await exist_user_validation(user);
  
  if(resultValidate.data.body.error !== undefined && resultValidate.data.body.error === 'noExist'){
    createqr(user);
  }else{
    console.log('********* resultValidate ', resultValidate.data.body);
    let step1 = document.getElementById('add-mfa-step-1');
    step1.classList.add('elementhide');
    let step2 = document.getElementById('add-mfa-step-2');
    step2.classList.remove('elementhide');
  }
}

const exist_user_validation = async (user) => {
  let url = "https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser";
  let headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
  return fetch(url, {
    "method": "POST",
    headers,
    "body": JSON.stringify({
      username: user,
      TWO_FA_APP_NAME: "trackIt",
      "operation": "verify_mfa_exist"
    })
  }).then(handleResponse)
  .then(response => ({success: true, data: response }))
  .then(error => ({success: false, ...error}))
  .catch(error => ({success: null, error}));


  /*return await fetch(url, {
    "method": "POST",
    headers,
    "body": JSON.stringify({
      username: user,
      TWO_FA_APP_NAME: "trackIt",
      "operation": "verify_mfa_exist"
    })
  }).then(handleResponse)
    .then(response => ({success: true, data: response }))
    .then(error => ({success: false, ...error}))
    .catch(error => ({success: null, error}));*/
};

// FmaAuthContainer Component
export class FmaAuthContainer extends Component {

  componentWillMount() {
    this.props.clear();
  }

  componentWillUnmount() {
    this.props.clear();
  }

  

  /*exist_user_validation(user, flag_new='default'){
    let url = 
    console.log('*********** user ',user);
    fetch("https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser", {
      "method": "POST",
      "headers": {
        "x-api-key": "VXgzfBKDnJ7S8PJhKJSjE47PUMgMQEvlptuF36Nd",
        "content-type": "application/json",
        "accept": "application/json"
      },
      "body": JSON.stringify({
        username: user,
        TWO_FA_APP_NAME: "trackIt",
        "operation": "verify_mfa_exist"
      })
    })
    .then(response => response.json())
    .then(response => {
      // eslint-disable-next-line no-undef
      let validateExist = response.body;
      console.log('***** validateExist.hasOwnProperty(username)!', validateExist.hasOwnProperty('username'));
      console.log('validateExist ***** ', validateExist);
      if(validateExist.hasOwnProperty('username') !== false){
        let step1 = document.getElementById('add-mfa-step-1');
        step1.classList.add('elementhide');
        if(flag_new === 'default'){
          let step2 = document.getElementById('add-mfa-step-2');
          step2.classList.remove('elementhide');
        }else{
          let step4 = document.getElementById('add-mfa-step-4');
          step4.classList.remove('elementhide');
          let tokenFMAId = document.getElementById('tokenFMAV');
          let wordsFMA = document.getElementById('wordsFMAV');
          tokenFMAId.value=validateExist.token;
          wordsFMA.value==validateExist.words;
          
        }
        return;
      }
    })
    .catch(err => {
      console.log(err);
      // eslint-disable-next-line react/jsx-no-undef
      return (<Redirect to="/"/>);
    });
  } */

  

  /*createqr2(user,password){
    let step1 = document.getElementById('add-mfa-step-1');
    if(step1 !== null){
      step1.classList.remove('elementhide');
      this.props.tokenResponse = '';
      let url = "http://" + window.location.hostname + ":8080/user/login";
      fetch(url, {
        "method": "POST",
        "headers": {
          "content-type": "application/json",
          "accept": "application/json"
        },
        "body": JSON.stringify({
          email: user,
          password: password,
          awsToken: null,
          origin: "trackit"
        })
      })
      .then(response => response.json())
      .then(response => {
        
        if(response.token){
          this.props.tokenResponse = response.token;
        }
        if(this.props.tokenResponse === '' || this.props.tokenResponse === null || this.props.tokenResponse === undefined){
          this.props.loginStatus = {"error":"The username or password is incorrect. Try again."}
          window.location.href = "/login";
        }else{
          let qrCodeString;
          let wordsString;
          let tokenFMA;
          fetch("https://cmg0yxsq0k.execute-api.us-west-2.amazonaws.com/dev/datauser", {
            "method": "POST",
            "headers": {
              "x-api-key": "VXgzfBKDnJ7S8PJhKJSjE47PUMgMQEvlptuF36Nd",
              "content-type": "application/json",
              "accept": "application/json"
            },
            "body": JSON.stringify({
              username: user,
              TWO_FA_APP_NAME: "trackIt",
              domainFMA: "localhost",
              "operation": "get_qrcode"
            })
          })
          .then(response => response.json())
          .then(response => {
            // eslint-disable-next-line no-undef
            qrCodeString = response.body.token_url;
            // eslint-disable-next-line no-undef
            wordsString = response.body.words;
            tokenFMA = response.body.token;
            // eslint-disable-next-line no-undef
            let QRCode = require('qrcode')
            let canvas = document.getElementById('qr')
            let tokenFMAId = document.getElementById('tokenFMA')
            let wordsFMA = document.getElementById('wordsFMA')
            QRCode.toCanvas(canvas, qrCodeString, function (error) {
              if (error) console.error(error)
                console.log('success!');
                tokenFMAId.value = tokenFMA;
                wordsFMA.value = wordsString;
                //this.props.tokenFMA = tokenFMA;
            })
          })
          .catch(err => {
            console.log(err);
            return (<Redirect to="/"/>);
          });
        }
        
      })
      .catch(err => {
        console.log('error login', err);
      });
    }
  }*/

  getVariableGetByName() {
    var variables = {};
    var arreglos = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
       variables[key] = value;
    });
    return variables;
 }

  render() {
    //let password = this.getVariableGetByName()["pswd"];
    let email = this.getVariableGetByName()["email"];
    
    if(email !== ''){
      createQrIfNotExist(email);
    }
    
    if (this.props.token)
      return (<Redirect to="/"/>);
    return (<Form
      submit={this.props.recover}
      recoverStatus={this.props.recoverStatus}
    />);
  }

}

FmaAuthContainer.propTypes = {
  recover: PropTypes.func.isRequired,
  token: PropTypes.string,
  recoverStatus: PropTypes.shape({
    status: PropTypes.bool,
    error: PropTypes.string
  })
};

/* istanbul ignore next */
const mapStateToProps = (state) => ({
  token: state.auth.token,
  recoverStatus: state.auth.recoverStatus
});

/* istanbul ignore next */
const mapDispatchToProps = (dispatch) => ({
  recover: (email) => {
    dispatch(Actions.Auth.recover(email))
  },
  clear: () => {
    dispatch(Actions.Auth.clearRecover());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(FmaAuthContainer);
