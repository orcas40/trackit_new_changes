import Login from './LoginContainer';
import Register from './RegisterContainer';
import Forgot from './ForgotContainer';
import Renew from './RenewContainer';
import FmaAuth from './FmaAuthContainer';

export default {
  Login,
  Register,
  Forgot,
  Renew,
  FmaAuth,
};
