module github.com/trackit/trackit

go 1.12

require (
	github.com/360EntSecGroup-Skylar/excelize v0.0.0-20190117023543-0c5c99e2ad14
	github.com/Bowery/prompt v0.0.0-20190916142128-fa8279994f75 // indirect
	github.com/aws/aws-sdk-go v0.0.0-20190117232950-b7ab18f9e850
	github.com/dchest/safefile v0.0.0-20151022103144-855e8d98f185 // indirect
	github.com/dgrijalva/jwt-go v0.0.0-20170608005149-a539ee1a749a
	github.com/fortytw2/leaktest v1.2.0 // indirect
	github.com/go-redis/redis v0.0.0-20190704095936-69cf7e5f6f35
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/go-cmp v0.5.2 // indirect
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510 // indirect
	github.com/google/uuid v1.2.0
	github.com/kardianos/govendor v1.0.9 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailru/easyjson v0.0.0-20180730094502-03f2033d19d5 // indirect
	github.com/olivere/elastic v6.2.21+incompatible
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v0.0.0-20170321230731-5bf94b69c6b6
	github.com/sha1sum/aws_signing_client v0.0.0-20200229211254-f7815c59d5c1
	github.com/stripe/stripe-go/v72 v72.3.0
	github.com/trackit/jsonlog v1.1.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/sys v0.0.0-20220204135822-1c1b9b1eba6a // indirect
	golang.org/x/tools v0.1.9 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
